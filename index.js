//console.log("hello")

let getCube = 2 ** 3;

let printCube = `The cube of 2 is ${getCube}`;
console.log(printCube);

let addArray = ['258', 'Washington Ave NW','California 90011'];

let [ deet1, deet2, deet3] = addArray;

let printAddress = `I live at ${deet1} ${deet2}, ${deet3}`;
console.log(printAddress);

let animalObject = {
	name: 'Lolong',
	type: 'saltwater crocodile',
	weight: '1057 kgs',
	measurement: '20 ft 3 in'

}

let {name, type, weight, measurement} = animalObject;

let printAnimal = `${name} was a ${type}. He weighed at ${weight} with a measurement of ${measurement}.`;

console.log(printAnimal);

let numArray = [1, 2, 3, 4, 5];



let numImplicit = numArray.forEach((loop) => {
	console.log(loop);
	return;
});





let reduceNumber = numArray.reduce((x,y) => x + y);
console.log(reduceNumber)




class Doggos {
	constructor(name, age, breed){
		this.name = name;
		this.age  = age;
		this.breed = breed;

	}
};

let doggo1 = new Doggos("Frankie", 5, "Miniature Dachshund");
console.log(doggo1);

let doggo2 = new Doggos("Guardian", 8, "German Sherpherd");
console.log(doggo2);

let doggo3 = new Doggos("Dirt Cloud", 3, "Poodle");
console.log(doggo3);
